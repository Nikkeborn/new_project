import random
import json


def gen_rand_user_data() -> dict:
	name: str = ''
	surname: str = ''
	age: str = ''
	
	let = (
	'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 
	'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 
	)
	num = ('1', '2', '3', '4', '5', '6', '7', '8', '9', '0',)

	while len(name) != 6:
		name += random.choice(let)

	while len(surname) != 6:
		surname += random.choice(let)

	while len(age) != 2:
		age += random.choice(num)

	user_dict: dict = {
	'name' : name,
	'surname' : surname,
	'age' : age,
	}
	return user_dict 	

print(gen_rand_user_data())

def mn(i: int) -> list:
	users_list = []
	j = 0
	while j != i:
		ud = gen_rand_user_data()
		users_list.append(ud)
		j += 1
	return users_list

init_user_list: list = mn(5)
print(init_user_list)
user_str: str = json.dumps(init_user_list)
print(user_str)
print(type(user_str))
us_li: list = json.loads(user_str)
print(us_li)
print(type(us_li))

init_user_list_2: list = mn(8)

with open('/home/wis/Desktop/users_file.json', 'w', encoding='utf-8') as uf:
	json.dump(init_user_list_2, uf, indent='	', ensure_ascii=True)

with open('/home/wis/Desktop/users_file.json', 'r') as read_uf:
	us_li_: list = json.load(read_uf)

print(us_li_)
print(type(us_li_))
print(bool(init_user_list_2 == us_li_))